#!/bin/bash -e
DESCRIPTION="Helper to manage a self-signed key pair to encrypt mini-buildd."

CN="${2:-$(hostname -f)}"
NAME="${3:-mini-buildd}"

DIR="/etc/ssl/mini-buildd"
KEY_DIR="${DIR}/private"
KEY="${KEY_DIR}/${NAME}.key"
CRT_DIR="${DIR}/certs"
CRT="${CRT_DIR}/${NAME}.crt"

usage()
{
	cat <<EOF
Usage: mini-buildd-self-signed-certificate status|create|remove [<cn> [<name>]]

${DESCRIPTION}

- <cn> defaults to current fully qualified host name.

- <name> defaults to 'mini-buildd'.

Key/cert files we manage:

- ${KEY}

- ${CRT}
EOF
}

case "${1}" in
	"--help")
		usage
	exit 0
esac


[ "$(id -u)" -eq 0 ] || { printf "E: Please run as root.\nTry 'mini-buildd-self-signed-certificate --help'.\n"; exit 2; }

status()
{
	printf "I: SSL/TLS self-signed: %s (%s)\n" "${CRT}" "${KEY}:"
	openssl x509 -in "${CRT}" -text
}

CNF="\
[ req ]
distinguished_name = req_distinguished_name
prompt             = no
policy             = policy_anything
req_extensions     = v3_req
x509_extensions    = v3_req

[ req_distinguished_name ]
commonName         = ${CN}

[ v3_req ]
basicConstraints   = CA:FALSE
subjectAltName     = DNS:${CN}
"

case ${1} in
	create)
		if [ ! -e "${KEY}" -o ! -e "${CRT}" ]; then
			mkdir --verbose --parents "${KEY_DIR}"
			mkdir --verbose --parents "${CRT_DIR}"
			openssl req -config <(printf "%s" "${CNF}") -new -x509 -days 3650 -nodes -sha256  -keyout "${KEY}" -out "${CRT}"
		fi
		chmod --verbose u=rwx,g=x,o= "${KEY_DIR}"
		if id --group mini-buildd >/dev/null; then
			chown --verbose root:mini-buildd "${KEY_DIR}"
			chown --verbose root:mini-buildd "${KEY}"
			chmod --verbose u=rw,g=r,o= "${KEY}"
		fi
		;;
	remove)
		printf "'${DIR}': "
		rm -r -v -I "${DIR}"
		;;
	status)
		status
		;;
	*)
		usage >&2
		exit 1
		;;
esac
