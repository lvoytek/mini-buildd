mini-buildd
===========

A custom build daemon for *Debian*-based distributions with
:ref:`all batteries included <abstract:Features>`.

.. toctree::
	 :maxdepth: 1

	 abstract

----------------------

.. toctree::
	 :maxdepth: 2

	 consumer
	 developer
	 administrator

----------------------

.. toctree::
	 :maxdepth: 1

	 auto_admonitions
	 roadmap
	 Python Package <python/modules>
