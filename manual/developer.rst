Developer's Manual
==================

This manual is aimed at all folks actually developing Debian packages.

Please read && act the :ref:`consumer:Consumer's Manual` first.

Introduction
------------

The core functionalities of mini-buildd are, 1st the
arch-multiplexed clean building, and 2nd providing a
repository. You don't need to worry about 1st, mini-buildd just
does it for you.

The 2nd however, the repository, goes public and hits "global
Debian namespace"; so, as a big picture, it's important first to
understand how mini-buildd's (default) setup tries to deal with
this.

First of all, each **instance** has it's own **identity**
string, which will be used in the name of the keyring package,
and will also appear in the apt repository in the ``Origin``
field.

Second, each instance instance may have ``N`` **repositories**,
which each have their own **identity** string, determining the
actual distribution names (``CODENAME-ID-SUITE``) to be used for
uploads or in apt lines.

Both identities should be "globally unique" to avoid any confusion or
conflicts with other existing repositories. Only exceptions are the
generic *Sandbox* and *Developer* repositories, with the de-facto
standard names ``test`` and ``debdev``; these should never be used
publicly or for anything but testing.

Third, when people are mixing repositories together, we want to avoid
package clashes, like same PACKAGE-VERSION from two different
repositories. Also, we want guaranteed upgradeability between two
different base distributions, and from experimental to
non-experimental suites. Hence, at least in the recommended
:ref:`consumer:Default Layout`, we also have a **version
restriction**, which resembles that of Debian Backports.


Setup
-----

dput
~~~~

(Some variant of) dput will be needed to upload packages. :debpkg:`dput-ng`
is recommended, as it also has support for ``ftps`` (which mini-buildd
might be setup for) since ``1.32``, see :debbug:`980735`

.. code:: bash
	 :class: root-code

	 apt install dput-ng

Traditional :debpkg:`dput` will also work fine when not using ftps (see :debbug:`980468`).

Additionally, there is :mbdcommand:`mini-buildd-dput` which should always work,
has some custom mini-buildd support but otherwise is only a minimal
subset of ``dput``.

You can retrieve a **config** snippet for dconf via the
:apicall:`dput_conf` -- just add that to your ``~/.dput.cf``.

For example, from the shell:

.. code:: bash

	 mini-buildd-api dput_conf <ENDPOINT> >>~/.dput.cf

.. tip:: Do mini-buildd commands support bash autocompletion?

	Yes, all commands written in python have full autocompletion support.

	You will get ``endpoint`` or ``dput target`` autocompletion after ``~/.dput.cf`` has been set up (see :ref:`developer:dput`).


User upload authorization
~~~~~~~~~~~~~~~~~~~~~~~~~

This assumes you already have a user account as detailed in :ref:`consumer:User`.

1. Upload your GnuPG key via the `user's profile </accounts/profile/>`_.
2. Get your administrator(s) to verifiy and approve your key
	 ((re)activates your user's Uploader instance) and to allow upload to
	 specific repositories by *changing* the user's Uploader instance.

You **don't need per user authorization**:

* In case the repository you upload for has **authorization disabled** completely (like in the sandbox repository ``test``).
* In case your key is included in the **per-repository predefined GnuPG keyrings** (by the administrator(s)).
* If a **different authorization theme** is place for your site, for example via :ref:`administrator:Auth via SSH wrapper`.


Uploading packages
------------------

Just like always, via ``dput``. For the default configuration you get
via :apicall:`dput_conf` it's something like:

.. code:: bash

	 dput mini-buildd-ARCHIVE FOO.changes

.. tip:: Is package building parallel? (a.k.a. Where is the 'sbuild_jobs' settings?)

	The 'sbuild_jobs' setting (formerly in :ref:`administrator:Daemon`) is no longer used because:

		* The ``--jobs`` option of ``sbuild`` (resp. ``dpkg-buildpackage``) forces builds to be parallel (if N > 1), possible leading to unfixable build problems.
		* The ``--jobs`` option of ``sbuild`` (resp. ``dpkg-buildpackage``) forces builds to be non-parallel (if N = 1, the default). This is not what you want for most packages.
		* For some time now, ``DEB_BUILD_OPTIONS`` is set to ``parallel=<CPUS>`` by default by dpkg tools. Packages may act on this, and build parallel. Practically, most packages will also just do so by default, when using build tools like debhelper.

	I.e., we go completely with the defaults now. Use s.th. like ``dh --no-parallel`` in your rules if your package does **not** support parallel building.

	In case your really need or want to manipulate this, you can still set ``DEB_BUILD_OPTIONS`` via :ref:`developer:Upload Options`.


Upload Options
~~~~~~~~~~~~~~

An `Upload Option` is some value induced to mini-buildd via
special entries in the ``changelog`` of an upload. Thus, an upload
may overwrite some defaults, or request special handling.

Changelog entries denoting such an ``upload option`` need to be of the form::

	* MINI_BUILDD_OPTION: <key>[[<arch>]]=<value>

These options generally override resp. values (if any) in mini-buildd's configurations (for this one package build).

Please check docs of ``options`` argument in :apicall:`port` or :apicall:`port_ext` for a complete list of known ``Upload Options``.

.. versionadded:: 1.0.26

Example
.......

Consider an upload with this ``debian/changelog``::

	mini-buildd (1.0.25~test11+1) bullseye-test-unstable; urgency=medium

	  * Adds this.
	  * Adds that.
	  * Fixes something else.
	  * MINI_BUILDD_OPTION: lintian-mode=ignore
	  * MINI_BUILDD_OPTION: lintian-mode[armel]=disabled
	  * MINI_BUILDD_OPTION: deb-build-options=nocheck
	  * MINI_BUILDD_OPTION: auto-ports=buster-test-unstable

This would

* ignore lintian errors for this upload,
* not run lintian at all for builds on arch ``armel``
* not run any checks (via DEB_BUILD_OPTIONS=nocheck, see ``dpkg-buildpackge``).
* and finally (after successful install) do an automated port to ``buster``.

.. tip:: Is there a changelog mode to help with `Upload Options` for emacs?

	Yes, sort of -- at least (Mar 2022) as example. Try something like this in your emacs initialization::

		(setq mbd_archives '(test myarch))
		(load-file "/usr/share/doc/mini-buildd-utils/examples/mini-buildd.el")

	This adds a context menu when entering `Debian Changelog
	Mode`. Rudimentary still, but may already help.

Deprecated Notations
....................

.. deprecated:: 1.99.16

	 * ``ignore-lintian=True``: Use ``lintian-mode``.
	 * ``run-lintian=False``: Use ``lintian-mode``.

.. deprecated:: 1.0.26

	 * ``BACKPORT_MODE``: Use ``{lintian|piuparts|autopkgtest}-mode``.
	 * ``AUTO_BACKPORTS``: Use ``auto-ports``.

Control your package build results
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Check via web: :mbdpage:`events`.
* Check via shell: :mbdcommand:`mini-buildd-events`.
* Upload via :mbdcommand:`mini-buildd-dput` using ``--check-result`` option.
* Per notify (read: Email): A notification mail is sent to
	* *the uploader* (unless the repository is not configured to do so, or the mail address does not match the allowed list),
	* *any subscriber* or
	* your Email is configured by the administrator to always be notified for that repository.

.. tip:: Can I access built packages that have not been installed into the repository?

	 Yes, via :mbdpage:`builds-dir`.

	 Since 2.0, older build directories are cleared periodically via
	 internal cron, see :mbdpage:`crontab`, not immediately after the
	 build process.

Managing packages
-----------------

You can **view a source package** overview via the :apicall:`ls`.

You will find more options to manage packages like :apicall:`migrate`,
:apicall:`remove`, :apicall:`port` in this web page overview.


Advanced Topics
---------------

Porting packages ("automatic no-changes ports")
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You can automatically port packages already in the repository
(:apicall:`port`) as well as arbitrary external source packages
(:apicall:`port_ext`).

On the web interface, you find convenience support to trigger
**internal ports** from :apicall:`ls`, *external ports* on
:mbdpage:`repositories`.

*Internal ports* may also be triggered automatically on uploads via
``Upload Options`` (see :ref:`developer:Uploading packages`).

reprepro fails with debian/ as symlink in Debian native packages
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Please follow :debbug:`768046` for this subject.

In such a case, builds will be fine, but reprepro will not be
able to install the package; you will only be able to see
reprepro's error "No section and no priority for" in the
``daemon.log``.

For the moment, just avoid such a setup (which is imho not
desirable anyway). However, as it's a legal setup afaik it
should work after all.
