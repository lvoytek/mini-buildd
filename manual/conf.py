# http://www.sphinx-doc.org/en/stable/config.html
#
# pylint: disable=wrong-import-position,invalid-name,redefined-builtin  # Disable for file: These checks just can't be satisfied here.

import sys
import os
import glob
import pathlib
import re
import subprocess

# Add local package dir to path
sys.path.insert(0, os.path.abspath("../src"))

from mini_buildd import __version__, PyCompat, django_settings, config, api   # noqa (pep8 E402)

# Pseudo-configure django
django_settings.pseudo_configure()

# Autogenerate API documentation for package mini_buildd
# Strange to do this here, but it (works and) seems more coherent than extra support in setup.py
subprocess.check_call(["sphinx-apidoc", "--force", "--separate", "--output-dir", "./python/", "../src/mini_buildd/"])

# Autogenerate static HTML man pages for all commands
os.makedirs("_static/man", exist_ok=True)
COMMANDS = []
for manpage in glob.glob("../src/*.[18]"):
    name = os.path.splitext(os.path.basename(manpage))[0]
    html_file_path = os.path.join("_static/man", name + ".html")
    COMMANDS.append(name)
    print(f"Creating HTML manpage: {manpage} -> {html_file_path}")
    with open(html_file_path, "w", encoding="UTF-8") as f:
        # split magic: Remove CGI header produced by man2html
        f.write(subprocess.check_output(["man2html", "-M", "/static/manual/", manpage]).decode("UTF-8").split("\n", 2)[2])

# Autogenerate admonitions rst (hack)
ADMONITIONS = {
    "error": [],
    "attention": [],
    "note": [],
    "tip": [],
}

for gen in [[pathlib.Path(".").rglob("*.rst"), lambda fn: os.path.splitext(fn)[0]],
            # "../src/mini_buildd/models/base.py" -> "mini_buildd.models.base"
            [pathlib.Path("../src/").rglob("*.py"), lambda fn: "python/" + os.path.splitext(PyCompat.removeprefix(str(fn), "../src/"))[0].replace("/", ".").replace(".__init__", "")]]:
    for f_name in gen[0]:
        with open(f_name, encoding="UTF-8") as f:
            for line in f:
                m = re.match(rf".*\.\. ({'|'.join(ADMONITIONS.keys())}):: (.*)", line)
                if m:
                    print(f"Found admonition: {f_name} ({m[1]}): {m[2]}")
                    ADMONITIONS[m[1]].append([gen[1](f_name), m[2]])

with open("auto_admonitions.rst", "w", encoding="UTF-8") as f:
    f.write("""\
Admonitions
===========

Automated index of all admonitions (in this manual as well as in
python code documentation). Somewhat like a FAQ list.

For simplicity, we only use four admonitions:

* **error**: In- or external bugs (what *should* be fixed eventually somehow).
* **attention**: Something not obvious you should be aware of (but cannot really be fixed).
* **note**: Internal TODOS or other noteworthy stuff.
* **tip**: Free tip.
""")
    for typ, admonitions in ADMONITIONS.items():
        f.write(f"\n{typ}\n------------\n")
        for admonition in sorted(admonitions):
            f.write(f"* `{admonition[0]} <{admonition[0]}.html>`_: {admonition[1]}\n")

# Project data
project = "mini-buildd"
copyright = "2012-2022, mini-buildd maintainers"
version = __version__
release = __version__

# Config
extensions = [
    "sphinx.ext.extlinks",
    "sphinx.ext.autodoc",
    "sphinx.ext.graphviz",
    "sphinx.ext.autosectionlabel",
]
templates_path = ["_templates"]
source_suffix = ".rst"
master_doc = "index"
exclude_patterns = ["_build"]

rst_epilog = ""
for key, uri in config.URIS.items():
    uri = uri.get("view")
    if uri is not None:
        rst_epilog += f".. _{key}: {uri}\n"

# Style
html_theme = "bizstyle"  # Looks ok, has reasonable style support for admonitions.
html_logo = "../src/mini_buildd/static/mini_buildd.svg"
html_favicon = html_logo
html_theme_options = {
    "body_max_width": "none",   # Use full width
}
html_static_path = ["_static"]
html_sidebars = {
    "**": [
        "localtoc.html",
        "relations.html",
        "searchbox.html",
        "mbd-sidebar.html",  # custom
    ]
}
html_context = {
    "COMMANDS": COMMANDS,
    "APICALLS": api.CALLS,
}

# Extensions
extlinks = {
    "debbug": ("https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=%s", "Debian Bug #%s"),
    "debpkg": ("https://tracker.debian.org/pkg/%s", "Debian package '%s'"),
    "mbdpage": ("/mini_buildd/%s", "mini-buildd's '%s' page"),
    "mbdcommand": ("_static/man/%s.html", "command '%s'"),
    "apicall": ("/mini_buildd/api/?call=%s", "API call '%s'"),
}
autodoc_member_order = "bysource"   # Order members like in source (sphinx default (strangely enough) seems to be alphabetic order)
autosectionlabel_prefix_document = True
