-- AptKey constraints where introduced in experimental release 1.9.9.
-- They have been removed again in experimental release 1.9.14.
-- This may potentially create problems. At the very least, it's not in sync
-- with code (2.x should not change SQL schmema at all).
-- You may try to clear this up using s.th. like
--   cat remove-aptkey-constraints.sql | sqlite3 config.sqlite
-- Please test with a copy beforehand.

CREATE TABLE "mini_buildd_aptkey_no_constraints" (
	"id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	"extra_options" text NOT NULL,
	"pickled_data" text NOT NULL,
	"status" integer NOT NULL,
	"last_checked" datetime NOT NULL,
	"auto_reactivate" bool NOT NULL,
	"key_id" varchar(100) NOT NULL,
	"key" text NOT NULL, "key_long_id" varchar(254) NOT NULL,
	"key_created" varchar(254) NOT NULL,
	"key_expires" varchar(254) NOT NULL,
	"key_name" varchar(254) NOT NULL,
	"key_fingerprint" varchar(254) NOT NULL);

INSERT INTO mini_buildd_aptkey_no_constraints (id, extra_options, pickled_data, status, last_checked, auto_reactivate, key_id, key, key_long_id, key_created, key_expires, key_name, key_fingerprint)
	SELECT id, extra_options, pickled_data, status, last_checked, auto_reactivate, key_id, key, key_long_id, key_created, key_expires, key_name, key_fingerprint FROM mini_buildd_aptkey;
DROP TABLE mini_buildd_aptkey;
ALTER TABLE mini_buildd_aptkey_no_constraints RENAME TO mini_buildd_aptkey;
