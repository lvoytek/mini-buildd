;; Upload Options helper menu in Debian changelog mode
;;
;; This is already helpful, however:
;;
;; * should be added to changelog menu, rather
;; * ugly
;; * redundancy fest
;;
;; elisp cracks please fix and send patch...
;;
(defvar mbd_archives '(test))
(defvar mbd_codenames '(buster bullseye sid))
(defvar mbd_suites '(experimental snapshot unstable hotfix))
(setq mbd_distributions '())
(dolist (mbd mbd_archives)
	(dolist (codename mbd_codenames)
		(dolist (suite mbd_suites)
			(push (format "%s-%s-%s" codename mbd suite) mbd_distributions))))

(defun mini-buildd-debian-changelog-mode-hook ()

	(defvar mini-buildd-menu (make-sparse-keymap "mini-buildd"))
	(define-key global-map [menu-bar mini-buildd-menu] (cons "mini-buildd" mini-buildd-menu))
	(defvar mini-buildd-distributions-menu (make-sparse-keymap "mini-buildd-distributions"))
	(define-key global-map [menu-bar mini-buildd-menu mini-buildd-distributions-menu] (cons "Distributions" mini-buildd-distributions-menu))

	(defun mbd-ignore-lintian ()
		(interactive "*")
		(debian-changelog-add-entry)
		(insert "MINI_BUILDD_OPTION: lintian-mode=IGNORE"))
	(define-key mini-buildd-menu [mbd-ignore-lintian]
		'(menu-item "Ignore lintian" mbd-ignore-lintian))

	(defun mbd-ignore-autopkgtest ()
		(interactive "*")
		(debian-changelog-add-entry)
		(insert "MINI_BUILDD_OPTION: autopkgtest-mode=IGNORE"))
	(define-key mini-buildd-menu [mbd-ignore-autopkgtest]
		'(menu-item "Ignore autopkgtest" mbd-ignore-autopkgtest))

	(defun mbd-ignore-piuparts ()
		(interactive "*")
		(debian-changelog-add-entry)
		(insert "MINI_BUILDD_OPTION: piuparts-mode=IGNORE"))
	(define-key mini-buildd-menu [mbd-ignore-piuparts]
		'(menu-item "Ignore piuparts" mbd-ignore-piuparts))

	(defun mbd-auto-ports ()
		(interactive "*")
		(debian-changelog-add-entry)
		(insert "MINI_BUILDD_OPTION: auto-ports=<codenname>-<id>-<suite>,..."))
	(define-key mini-buildd-menu [mbd-auto-ports]
		'(menu-item "auto-ports" mbd-auto-ports))

	(defun mbd-internal-apt-priority ()
		(interactive "*")
		(debian-changelog-add-entry)
		(insert "MINI_BUILDD_OPTION: internal-apt-priority=<n>"))
	(define-key mini-buildd-menu [mbd-internal-apt-priority]
		'(menu-item "internal-apt-priority" mbd-internal-apt-priority))

	(defun mbd-deb-build-options ()
		(interactive "*")
		(debian-changelog-add-entry)
		(insert "MINI_BUILDD_OPTION: deb-build-options[<arch>]=<option> <option>..."))
	(define-key mini-buildd-menu [mbd-deb-build-options]
		'(menu-item "deb-build-options" mbd-deb-build-options))

	(defun mbd-deb-build-profiles ()
		(interactive "*")
		(debian-changelog-add-entry)
		(insert "MINI_BUILDD_OPTION: deb-build-profiles[<arch>]=<profile> <profile>..."))
	(define-key mini-buildd-menu [mbd-deb-build-profiles]
		'(menu-item "deb-build-profiles" mbd-deb-build-profiles))

	(dolist (dist mbd_distributions)
		(define-key mini-buildd-distributions-menu `[,dist]
			(cons dist `(lambda () (interactive) (debian-changelog-setdistribution ,dist))))
		)
	)

(add-hook 'debian-changelog-mode-hook 'mini-buildd-debian-changelog-mode-hook)
